package com.example.myproductsapp.views.main_view_tools;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.myproductsapp.models.entities.Product;

import java.util.ArrayList;

public class ProductAdapter extends BaseAdapter
{

    private ArrayList<Product> products;

    Context context;
    LayoutInflater layoutInflater;

    public ProductAdapter(ArrayList<Product> products, Context context)
    {
        this.context = context;
        this.products = products;

        layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int i) {
        return products.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        if (v==null)
        {
            v = layoutInflater.inflate(android.R.layout.simple_list_item_1, viewGroup, false);
        }

        Product product = products.get(i);

        TextView textView = v.findViewById(android.R.id.text1);

        String str = String.format("Продукт: %s \n Цена: %d", product.getName(), product.getPrice());

        textView.setText(str);

        return v;
    }
}
