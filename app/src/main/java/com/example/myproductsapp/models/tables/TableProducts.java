package com.example.myproductsapp.models.tables;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.myproductsapp.models.entities.Product;
import com.example.myproductsapp.models.tools.DbHelper;

import java.util.ArrayList;

public class TableProducts
{
    private DbHelper dbHelper;

    public TableProducts(DbHelper dbHelper)
    {
        this.dbHelper = dbHelper;
    }

    public ArrayList<Product> getAll()
    {
        ArrayList<Product> products = new ArrayList<>();

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String sqlCommand = "SELECT * FROM `products`";

        Cursor cursor = db.rawQuery(sqlCommand, null);

        while (cursor.moveToNext()==true)
        {
            int id = cursor.getInt(0);
            String name = cursor.getString(1);
            String picturePath= cursor.getString(2);
            int price=cursor.getInt(3);

            Product product = new Product(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getInt(3)
            );

            products.add(product);
        }

        cursor.close();

        dbHelper.close();

        return products;
    }

    public ArrayList<Product> GetByPartOfName(String partOfName)
    {
        ArrayList<Product> products = new ArrayList<>();

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String sqlCommand = "SELECT * FROM products WHERE name like '%"+partOfName+"%'";

        Cursor cursor = db.rawQuery(sqlCommand,null);

        while (cursor.moveToNext() == true)
        {
            Product product = new Product(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getInt(3)
            );

            products.add(product);
        }

        cursor.close();
        dbHelper.close();

        return products;
    }
}

